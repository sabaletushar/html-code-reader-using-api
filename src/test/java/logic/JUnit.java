package logic;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class JUnit {

    @org.junit.jupiter.api.Test
    void loadingHtmlFile() {
        Service s1 = new Service();
        int b = s1.loadingHtmlFile("https://www.google.com");
        assertEquals(1,b);
    }
}